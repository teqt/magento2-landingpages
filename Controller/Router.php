<?php

namespace Teqt\LandingPages\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Teqt\LandingPages\Api\LandingPageRepositoryInterface;

class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * @var LandingPageRepositoryInterface
     */
    protected $landingPageRepository;

    /**
     * Router constructor.
     * @param ActionFactory $actionFactory
     * @param LandingPageRepositoryInterface $landingPageRepository
     */
    public function __construct(ActionFactory $actionFactory, LandingPageRepositoryInterface $landingPageRepository)
    {
        $this->actionFactory = $actionFactory;
        $this->landingPageRepository = $landingPageRepository;
    }

    /**
     * Match application action by request
     * @param RequestInterface $request
     * @return ActionInterface
     */
    public function match(RequestInterface $request)
    {
        $url    = trim($request->getPathInfo());
        $parts  = explode('/', ltrim($url, '/'));
        $slug   = array_shift($parts);

        $landingPage = $this->landingPageRepository->findBySlug($slug);
        if(! $landingPage || ! $landingPage->getId())
        {
            return null;
        }

        $request->setModuleName('landingpages')
            ->setControllerName('landingpage')
            ->setActionName('view')
            ->setParam('landing_page_id', $landingPage->getId());

        $request->setAlias(\Magento\Framework\Url::REWRITE_REQUEST_PATH_ALIAS, $request->getPathInfo());
        return $this->actionFactory->create(Forward::class, [
            'request' => $request
        ]);
    }
}
