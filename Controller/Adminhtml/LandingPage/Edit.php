<?php

namespace Teqt\LandingPages\Controller\Adminhtml\LandingPage;

use Magento\Backend\App\Action;
use Teqt\LandingPages\Model\LandingPage;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * Add a landing page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->getRequest()->setParam('landing_page_id',
            $this->getRequest()->getParam('id'));

        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $data = $this->getRequest()->getParam('landingpage');
        if (is_array($data)) {
            $data['config'] = json_encode($data['config']);

            $landingPage = $this->_objectManager->create(LandingPage::class);
            $landingPage->setData($data)
                ->save();

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
    }
}
