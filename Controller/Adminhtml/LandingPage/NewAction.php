<?php

namespace Teqt\LandingPages\Controller\Adminhtml\LandingPage;

use Teqt\LandingPages\Model\LandingPage;

class NewAction extends Edit
{
    /**
     * Add a landing page
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();

        $data = $this->getRequest()->getParam('landingpage');
        if (is_array($data)) {
            $landingPage = $this->_objectManager->create(LandingPage::class);
            $landingPage->setData($data)
                ->save();

            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index');
        }
    }
}
