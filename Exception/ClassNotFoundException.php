<?php

namespace Teqt\LandingPages\Exception;

/**
 * Class ClassNotFoundException
 *
 * Since we shouldn't use the Zend Framework API in our Magento
 * plugin, introduce an own exception for missing or unconfigured
 * classes.
 *
 * @package Teqt\LandingPages\Exception
 */
class ClassNotFoundException extends \Exception
{
}