<?php

namespace Teqt\LandingPages\Exception;

/**
 * Class MissingArgumentException
 * @package Teqt\LandingPages\Exception
 */
class MissingArgumentException extends \Exception
{
}
