<?php

namespace Teqt\LandingPages\Block\LandingPage;

class ContentHeading extends AbstractBlock
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        return $this->filterProvider->getPageFilter()
            ->filter($this->getLandingPage()->getContentHeading());
    }
}
