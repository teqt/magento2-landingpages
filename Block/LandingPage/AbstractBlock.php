<?php

namespace Teqt\LandingPages\Block\LandingPage;

use Magento\Framework\View\Element\AbstractBlock as BaseAbstractBlock;
use Teqt\LandingPages\Api\LandingPageRepositoryInterface;
use Teqt\LandingPages\Api\Data\LandingPageInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Cms\Model\Template\FilterProvider;
use Teqt\LandingPages\Exception\ClassNotFoundException;
use Teqt\LandingPages\Exception\MissingArgumentException;

abstract class AbstractBlock extends BaseAbstractBlock
{
    /**
     * @var LandingPageInterface
     */
    protected $landingPage;

    /**
     * @var FilterProvider
     */
    protected $filterProvider;

    /**
     * Content constructor.
     * @param Context $context
     * @param LandingPageRepositoryInterface $landingPageRepository
     * @param array $data
     * @throws MissingArgumentException
     * @throws ClassNotFoundException
     */
    public function __construct(Context $context, LandingPageRepositoryInterface $landingPageRepository, FilterProvider $filterProvider, array $data = [])
    {
        $this->landingPage      = $landingPageRepository->findByRequest();
        $this->filterProvider   = $filterProvider;

        parent::__construct($context, $data);
    }

    /**
     * @return LandingPageInterface
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }
}
