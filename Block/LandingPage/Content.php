<?php

namespace Teqt\LandingPages\Block\LandingPage;

class Content extends AbstractBlock
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        return $this->filterProvider->getPageFilter()
            ->filter($this->getLandingPage()->getContent());
    }
}
