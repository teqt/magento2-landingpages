<?php

namespace Teqt\LandingPages\Block\LandingPage;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\ListProduct as BaseListProduct;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\Helper\Data;
use Teqt\LandingPages\Api\Data\LandingPageInterface;
use Teqt\LandingPages\Api\LandingPageRepositoryInterface;

class ListProduct extends BaseListProduct
{
    /**
     * @var LandingPageInterface
     */
    protected $landingPage;

    /**
     * ListProduct constructor.
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param LandingPageRepositoryInterface $landingPageRepository
     * @param array $data
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     * @throws \Teqt\LandingPages\Exception\MissingArgumentException
     */
    public function __construct(Context $context, PostHelper $postDataHelper, Resolver $layerResolver, CategoryRepositoryInterface $categoryRepository, Data $urlHelper, LandingPageRepositoryInterface $landingPageRepository, array $data = [])
    {
        $this->landingPage = $landingPageRepository->findByRequest();
        parent::__construct($context, $postDataHelper, $layerResolver, $categoryRepository, $urlHelper, $data);
    }

    /**
     * @return LandingPageInterface
     */
    public function getLandingPage()
    {
        return $this->landingPage;
    }

    /**
     * @inheritdoc
     */
    protected function _getProductCollection()
    {
        if ($this->_productCollection === null) {
            $this->_productCollection = $this->_initializeProductCollection();
        }

        return $this->_productCollection;
    }

    /**
     * Unfortunately, the original function is private, so we have to explicitly rename the function to make it work.
     *
     * @return Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _initializeProductCollection()
    {
        /* @var Layer $layer */
        $layer = $this->getLayer();

        /** Set root category to be the current category */
        $this->setCategoryId($this->_storeManager->getStore()->getRootCategoryId());
        $category = $this->categoryRepository->get($this->getCategoryId());
        $layer->setCurrentCategory($category);

        $collection = $layer->getProductCollection();
        $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

        $config = $this->getLandingPage()->getConfig();
        foreach ($config as $attribute => $values) {
            if (empty($values)) {
                continue;
            }

            if($attribute === '__attribute_set') {
                $collection->addAttributeToFilter('attribute_set_id', ['in' => $values]);
                continue;
            }

            $collection->joinAttribute($attribute, 'catalog_product/' . $attribute, 'entity_id', null, 'left')
                ->addAttributeToFilter($attribute, ['in' => $values]);
        }

        /** Toolbar is unfortunately also private */
        //$this->addToolbarBlock($collection);

        $this->_eventManager->dispatch(
            'catalog_block_product_list_collection',
            ['collection' => $collection]
        );

        return $collection;
    }
}
