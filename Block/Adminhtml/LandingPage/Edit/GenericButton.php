<?php

namespace Teqt\LandingPages\Block\Adminhtml\LandingPage\Edit;

use Magento\Backend\Block\Widget\Context;
use Teqt\LandingPages\Api\LandingPageRepositoryInterface;
use Teqt\LandingPages\Exception\ClassNotFoundException;
use Teqt\LandingPages\Exception\MissingArgumentException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var LandingPageRepositoryInterface
     */
    protected $landingPageRepository;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     */
    public function __construct(Context $context, LandingPageRepositoryInterface $landingPageRepository)
    {
        $this->urlBuilder = $context->getUrlBuilder();
        $this->landingPageRepository = $landingPageRepository;
    }

    /**
     * @return mixed|null
     *
     * @throws ClassNotFoundException
     * @throws MissingArgumentException
     */
    public function getId()
    {
        try {
            $landingPage = false;
            $landingPage = $this->landingPageRepository->findByRequest();
        }
        catch(MissingArgumentException $e) {}

        return $landingPage ? $landingPage->getId() : null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
