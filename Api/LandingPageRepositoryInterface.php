<?php

namespace Teqt\LandingPages\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Teqt\LandingPages\Api\Data\LandingPageInterface;
use Teqt\LandingPages\Exception\ClassNotFoundException;
use Teqt\LandingPages\Exception\MissingArgumentException;

interface LandingPageRepositoryInterface
{
    /**
     * @param int $id
     * @return AbstractModel
     */
    public function findById($id);

    /**
     * @param string $slug
     * @return LandingPageInterface
     */
    public function findBySlug($slug);

    /**
     * @return AbstractModel|LandingPageInterface
     * @throws MissingArgumentException
     * @throws ClassNotFoundException
     */
    public function findByRequest();

    /**
     * @param SearchCriteriaInterface $criteria
     * @return AbstractCollection
     */
    public function findAll(SearchCriteriaInterface $criteria);

    /**
     * @param LandingPageInterface $landingPage
     * @throws \Exception
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(LandingPageInterface $landingPage);
}
