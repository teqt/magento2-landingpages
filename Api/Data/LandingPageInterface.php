<?php

namespace Teqt\LandingPages\Api\Data;

/**
 * Interface LandingPageInterface
 */
interface LandingPageInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return array
     */
    public function getConfig();
}
