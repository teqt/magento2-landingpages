<?php

namespace Teqt\LandingPages\Ui\Component\Form\LandingPage;

use Magento\Framework\View\Element\UiComponent\ContextInterface;  
use Magento\Framework\View\Element\UiComponentInterface;  
use Magento\Ui\Component\Form\FieldFactory;  
use Magento\Ui\Component\Form\Fieldset;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory as AttributeSetCollectionFactory;
use Teqt\LandingPages\Repository\LandingPageRepository;

class AttributeFieldset extends Fieldset 
{
    /**
     * @var FieldFactory
     */
    protected $fieldFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var CollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * @var AttributeSetCollectionFactory
     */
    protected $attributeSetCollectionFactory;

    /**
     * @var LandingPageRepository
     */
    protected $landingPageRepository;

    /**
     * AttributeFieldset constructor.
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     * @param FieldFactory $fieldFactory
     */
    public function __construct(ContextInterface $context, array $components = [], array $data = [],
        FieldFactory $fieldFactory, 
        ScopeConfigInterface $scopeConfig, 
        CollectionFactory $attributeCollectionFactory, 
        AttributeSetCollectionFactory $attributeSetCollectionFactory, 
        LandingPageRepository $landingPageRepository
    )
    {
        parent::__construct($context, $components, $data);

        $this->fieldFactory                     = $fieldFactory;
        $this->scopeConfig                      = $scopeConfig;
        $this->attributeCollectionFactory       = $attributeCollectionFactory;
        $this->attributeSetCollectionFactory    = $attributeSetCollectionFactory;
        $this->landingPageRepository            = $landingPageRepository;
    }

    /**
     * Get components
     *
     * @return UiComponentInterface[]
     */
    public function getChildComponents()
    {
        $available = $this->scopeConfig->getValue('cms/teqt_landingpages/display_text');
        if($available) {
            $available = explode(',', $available);
        } else {
            $available = '_x-none';
        }

        $current    = $this->landingPageRepository->findByRequest();
        $config     = $current->getConfig();

        $attributeSets = $this->attributeSetCollectionFactory->create()
            ->addFieldToFilter(\Magento\Eav\Model\Entity\Attribute\Set::KEY_ENTITY_TYPE_ID, 4)
            ->addOrder('sort_order')
            ->getItems();

        $field = $this->fieldFactory->create();
        $name = '__attribute_set';

        $field->setData([
            'name'      => $name,
            'options'   => array_map(function($attributeSet) {
                return [
                    'value' => $attributeSet->getAttributeSetId(),
                    'label' => $attributeSet->getAttributeSetName(),
                ];
            }, $attributeSets),
            'config'    => [
                'label'         => __('Attribute sets'),
                'value'         => (array_key_exists($name, $config) ? $config[$name] : []),
                'formElement'   => 'multiselect',
            ],
        ]);

        $field->prepare();
        $this->addComponent($name, $field);

        $attributes = $this->attributeCollectionFactory->create()
            ->addFieldToFilter( 'attribute_code', [ 'in' => $available ] )
            ->addFieldToSelect( '*' );

        foreach ($attributes as $attribute)
        {
            if(! in_array($attribute->getFrontendInput(), ['select', 'multiselect']))
            {
                continue;
            }

            $field = $this->fieldFactory->create();
            $name = $attribute->getAttributeCode();

            $field->setData([
                'name'      => $name,
                'options'   => $attribute->getSource()->getAllOptions(),
                'config'    => [
                    'label'         => $attribute->getFrontendLabel(),
                    'value'         => (array_key_exists($name, $config) ? $config[$name] : []),
                    'formElement'   => 'multiselect',
                ],
            ]);

            $field->prepare();
            $this->addComponent($name, $field);
        }

        return parent::getChildComponents();
    }
}
