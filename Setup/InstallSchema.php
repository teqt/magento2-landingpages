<?php

namespace Teqt\LandingPages\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Install required Database schema for this table
 * @package Teqt\LandingPages\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install required tables for this module
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable('teqt_landing_page');
        if( $setup->getConnection()->isTableExists($tableName) !== true)
        {
            $definition = $this->createLandingPageTableDefinition($setup, $tableName);
            $setup->getConnection()->createTable($definition);
        }

        $setup->endSetup();
    }

    /**
     * Create answer table definition for setup
     * @param SchemaSetupInterface $setup
     * @param string $tableName
     * @return Table
     * @throws \InvalidArgumentException
     * @throws \Zend_Db_Exception
     */
    protected function createLandingPageTableDefinition(SchemaSetupInterface $setup, $tableName)
    {
        if( ! is_string($tableName))
        {
            throw new \InvalidArgumentException( "Parameter \$tableName should be a string." );
        }

        $storeTable = $setup->getTable('store');
        return $setup->getConnection()
            ->newTable($tableName)
            ->addColumn('id', Table::TYPE_INTEGER, null, [
                TABLE::OPTION_IDENTITY  => true,
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_PRIMARY   => true,
                TABLE::OPTION_NULLABLE  => false,
            ])
            ->addColumn('is_active', Table::TYPE_BOOLEAN, null, [
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => '1'
            ])
            ->addColumn('store_id', Table::TYPE_SMALLINT, null, [
                TABLE::OPTION_UNSIGNED  => true,
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => '0'
            ])
            ->addColumn('title', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => ''
            ])
            ->addColumn('meta_title', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => true
            ])
            ->addColumn('meta_keywords', Table::TYPE_TEXT, 65536, [
                TABLE::OPTION_NULLABLE  => true
            ])
            ->addColumn('meta_description', Table::TYPE_TEXT, 65536, [
                TABLE::OPTION_NULLABLE  => true
            ])
            ->addColumn('slug', Table::TYPE_TEXT, 255, [
                TABLE::OPTION_NULLABLE  => false
            ])
            ->addColumn('content_heading', Table::TYPE_TEXT, 65536, [
                TABLE::OPTION_NULLABLE  => true,
                TABLE::OPTION_DEFAULT   => ''
            ])
            ->addColumn('content', Table::TYPE_TEXT, 16777216, [
                TABLE::OPTION_NULLABLE  => true,
                TABLE::OPTION_DEFAULT   => ''
            ])
            ->addColumn('config', Table::TYPE_TEXT, 65536, [
                TABLE::OPTION_NULLABLE  => false
            ])
            ->addColumn('created_at', Table::TYPE_TIMESTAMP, null, [
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => TABLE::TIMESTAMP_INIT
            ])
            ->addColumn('updated_at', Table::TYPE_TIMESTAMP, null, [
                TABLE::OPTION_NULLABLE  => false,
                TABLE::OPTION_DEFAULT   => TABLE::TIMESTAMP_INIT_UPDATE
            ])
            ->addIndex(
                $setup->getIdxName($tableName, ['slug', 'store_id', 'is_active'], AdapterInterface::INDEX_TYPE_UNIQUE),
                ['slug', 'store_id', 'is_active'],
                ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
            )
            ->addForeignKey(
                $setup->getFkName($tableName, 'store_id', $storeTable, 'store_id'),
                'store_id', $storeTable, 'store_id',
                Table::ACTION_CASCADE
            );
    }
}