<?php

namespace Teqt\LandingPages\Repository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Teqt\LandingPages\AbstractFactory;
use Teqt\LandingPages\Model\AbstractModelFactory;

abstract class AbstractRepository
{
    /**
     * @var AbstractDb
     */
    protected $resource;

    /**
     * @var AbstractCollection
     */
    protected $collectionFactory;

    /**
     * @var CollectionProcessor
     */
    protected $collectionProcessor;

    /**
     * @var AbstractModelFactory
     */
    protected $factory;

    /**
     * @var array
     */
    protected $instances = [];

    /**
     * AbstractRepository constructor.
     * @param AbstractDb $resource
     * @param AbstractFactory $collectionFactory
     * @param CollectionProcessor $collectionProcessor
     * @param AbstractModelFactory $factory
     */
    public function __construct(
        AbstractDb $resource,
        AbstractFactory $collectionFactory,
        CollectionProcessor $collectionProcessor,
        AbstractModelFactory $factory
    )
    {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->factory = $factory;
    }

    /**
     * @param int $id
     * @return AbstractModel
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     */
    public function findById($id, $renew = false)
    {
        if( array_key_exists($id, $this->instances) && ! $renew)
        {
            return $this->instances[$id];
        }

        $this->instances[$id] = $this->_findBy($id, 'id');
        return $this->instances[$id];
    }

    /**
     * @param SearchCriteriaInterface $criteria
     * @return mixed
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     */
    public function findAll(SearchCriteriaInterface $criteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);

        return $collection;
    }

    /**
     * @param mixed $value
     * @param string $field
     * @return AbstractModel
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     */
    protected function _findBy($value, $field)
    {
        $model = $this->factory->create();
        $model->load($value, $field);

        if(! $model->getId())
        {
            return false;
        }

        return $model;
    }

    /**
     * @param mixed $question
     * @throws \Exception
     */
    protected function _save($question)
    {
        $this->resource->save($question);
    }
}