<?php

namespace Teqt\LandingPages\Repository;

use Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Model\AbstractModel;
use Teqt\LandingPages\Api\LandingPageRepositoryInterface;
use Teqt\LandingPages\Api\Data\LandingPageInterface;
use Teqt\LandingPages\Exception\ClassNotFoundException;
use Teqt\LandingPages\Exception\MissingArgumentException;
use Teqt\LandingPages\Model\LandingPageInterfaceFactory;
use Teqt\LandingPages\Model\Resource\LandingPage as LandingPageResource;

/**
 * Class LandingPageRepository
 */
class LandingPageRepository extends AbstractRepository implements LandingPageRepositoryInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * LandingPageRepository constructor.
     * @param LandingPageResource $resource
     * @param LandingPageResource\CollectionFactory $collectionFactory
     * @param CollectionProcessor $collectionProcessor
     * @param LandingPageInterfaceFactory $factory
     * @param RequestInterface $request
     */
    public function __construct(
        LandingPageResource $resource,
        LandingPageResource\CollectionFactory $collectionFactory,
        CollectionProcessor $collectionProcessor,
        LandingPageInterfaceFactory $factory,
        RequestInterface $request
    )
    {
        $args = func_get_args();
        $this->request = array_pop($args);
        parent::__construct(...$args);
    }

    /**
     * @param string $slug
     * @return AbstractModel|LandingPageInterface
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     */
    public function findBySlug($slug)
    {
        return $this->_findBy($slug, 'slug');
    }

    /**
     * @param string $email
     * @return AbstractModel|LandingPageInterface
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     */
    public function findByEmail($email)
    {
        return $this->_findBy($email, 'email');
    }

    /**
     * @return AbstractModel|LandingPageInterface
     * @throws MissingArgumentException
     * @throws ClassNotFoundException
     */
    public function findByRequest()
    {
        if(! ($landingPageId = $this->request->getParam('landing_page_id', false)))
        {
            throw new MissingArgumentException("Landing page id is not set in current request.");
        }

        return $this->findById($landingPageId);
    }

    /**
     * @param LandingPageInterface $landingPage
     * @throws \Exception
     */
    public function save(LandingPageInterface $landingPage)
    {
        $this->_save($landingPage);
    }
}
