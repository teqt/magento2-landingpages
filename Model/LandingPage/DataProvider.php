<?php

namespace Teqt\LandingPages\Model\LandingPage;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Teqt\LandingPages\Api\Data\LandingPageInterface;
use Teqt\LandingPages\Model\Resource\LandingPage\CollectionFactory;

class DataProvider extends AbstractDataProvider
{
    /**
     * DataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $landingPageCollectionFactory
     * @param array $meta
     * @param array $data
     *
     * @throws \Teqt\LandingPages\Exception\ClassNotFoundException
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $landingPageCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $landingPageCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $landingPages = $this->collection->getItems();

        $this->loadedData = [];

        /** @var LandingPageInterface $landingPage */
        foreach ($landingPages as $landingPage) {
            $this->loadedData[$landingPage->getId()]['landingpage'] = $landingPage->getData();
        }

        return $this->loadedData;

    }
}
