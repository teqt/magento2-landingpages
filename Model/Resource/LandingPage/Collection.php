<?php

namespace Teqt\LandingPages\Model\Resource\LandingPage;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Teqt\LandingPages\Model\LandingPage;
use Teqt\LandingPages\Model\Resource\LandingPage as LandingPageResource;

class Collection extends AbstractCollection
{
    /**
     * Define landing page collection
     */
    protected function _construct()
    {
        $this->_init(LandingPage::class, LandingPageResource::class);
    }
}
