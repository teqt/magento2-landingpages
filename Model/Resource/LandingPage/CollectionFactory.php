<?php

namespace Teqt\LandingPages\Model\Resource\LandingPage;

use Teqt\LandingPages\AbstractFactory;

class CollectionFactory extends AbstractFactory
{
    /**
     * @return string
     */
    public function context()
    {
        return Collection::class;
    }
}