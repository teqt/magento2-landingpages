<?php

namespace Teqt\LandingPages\Model\Resource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class LandingPage extends AbstractDb
{
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('teqt_landing_page', 'id');
    }
}