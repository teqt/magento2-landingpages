<?php

namespace Teqt\LandingPages\Model;

use Teqt\LandingPages\Api\Data\LandingPageInterface;

class LandingPageInterfaceFactory extends AbstractModelFactory
{
    /**
     * @return string
     */
    public function context()
    {
        return LandingPageInterface::class;
    }
}