<?php

namespace Teqt\LandingPages\Model;

use Magento\Framework\Model\AbstractModel;
use Teqt\LandingPages\Api\Data\LandingPageInterface;
use Teqt\LandingPages\Model\Resource\LandingPage as LandingPageResource;

class LandingPage extends AbstractModel implements LandingPageInterface
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(LandingPageResource::class);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        if(! ($config = json_decode(parent::getConfig(), true))) {
            return [];
        }

        return $config;
    }
}
