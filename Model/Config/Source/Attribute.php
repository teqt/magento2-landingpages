<?php

namespace Teqt\LandingPages\Model\Config\Source;

use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;

/**
 * Config attribute source
 *
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class Attribute implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Attribute collection factory
     *
     * @var CollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * Construct
     *
     * @param CollectionFactory $attributeCollectionFactory
     */
    public function __construct(CollectionFactory $attributeCollectionFactory)
    {
        $this->attributeCollectionFactory = $attributeCollectionFactory;
    }

    /**
     * Return option array
     *
     * @param bool $addEmpty
     * @return array
     */
    public function toOptionArray($addEmpty = true)
    {
        $collection = $this->attributeCollectionFactory->create();

        $collection->addFieldToSelect(['attribute_code'])->load();

        $options = [];

        if ($addEmpty) {
            $options[] = [
                'value' => '',
                'label' => __('-- Please Select an Attribute --'), 
            ];
        }
        foreach ($collection as $attribute) {
            $options[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getAttributeCode(), 
            ];
        }

        return $options;
    }
}
