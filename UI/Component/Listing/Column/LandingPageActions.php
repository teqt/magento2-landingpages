<?php

namespace Teqt\LandingPages\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class DepartmentActions
 */
class LandingPageActions extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['edit'] = [
                    'href' => $this->getEditUrl($item),
                    'label' => __('Edit'),
                    'hidden' => false,
                ];
                $item[$this->getData('name')]['delete'] = [
                    'href' => $this->getDeleteUrl($item),
                    'label' => __('Delete'),
                    'onclick' => 'deleteConfirm(\''
                        . __('Are you sure you want to delete this landing page?')
                        . '\', \'' . $this->getDeleteUrl($item) . '\')',
                    'hidden' => false,
                ];
            }
        }

        return $dataSource;
    }

    /**
     * @param array $item
     * @return string
     */
    public function getEditUrl(array $item)
    {
        return $this->urlBuilder->getUrl(
            'teqt_landingpages/landingpage/edit',
            ['id' => $item['id']]
        );
    }

    /**
     * @param array $item
     * @return string
     */
    public function getDeleteUrl(array $item)
    {
        return $this->urlBuilder->getUrl(
            'teqt_landingpages/landingpage/delete',
            ['id' => $item['id']]
        );
    }
}
