<?php

namespace Teqt\LandingPages\Ui\Component\Form\LandingPage;

use Magento\Framework\View\Element\UiComponent\ContextInterface;  
use Magento\Framework\View\Element\UiComponentInterface;  
use Magento\Ui\Component\Form\FieldFactory;  
use Magento\Ui\Component\Form\Fieldset;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;

class AttributeFieldset extends Fieldset 
{
    /**
     * @var FieldFactory
     */
    protected $fieldFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var CollectionFactory
     */
    protected $attributeCollectionFactory;

    /**
     * AttributeFieldset constructor.
     * @param ContextInterface $context
     * @param array $components
     * @param array $data
     * @param FieldFactory $fieldFactory
     */
    public function __construct(ContextInterface $context, array $components = [], array $data = [],
        FieldFactory $fieldFactory, ScopeConfigInterface $scopeConfig, CollectionFactory $attributeCollectionFactory)
    {
        parent::__construct($context, $components, $data);

        $this->fieldFactory = $fieldFactory;
        $this->scopeConfig = $scopeConfig;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
    }

    /**
     * Get components
     *
     * @return UiComponentInterface[]
     */
    public function getChildComponents()
    {
        $available = $this->scopeConfig->getValue('cms/teqt_landingpages/display_text');
        if($available) {
            $available = explode(',', $available);
        } else {
            $available = '_x-none';
        }

        $attributes = $this->attributeCollectionFactory->create()
            ->addFieldToFilter( 'attribute_code', [ 'in' => $available ] )
            ->addFieldToSelect( '*' );
        foreach ($attributes as $attribute)
        {
            if(! in_array($attribute->getFrontendInput(), ['select', 'multiselect']))
            {
                continue;
            }

            $field = $this->fieldFactory->create();
            $name = $attribute->getAttributeCode();

            var_dump($attribute->getSource()->getAllOptions());
            die();
            $field->setData([
                'name'      => $name,
                'options'   => [[
                    'value'     => 'a',
                    'label'     => 'Bla'
                ], [ 
                    'value'     => 'b',
                    'label'     => 'Bladibla'
                ]],
                'config'    => [
                    'label'         => $attribute->getFrontendLabel(),
                    'value'         => '',
                    'formElement'   => 'multiselect',
                ],
            ]);

            $field->prepare();
            $this->addComponent($name, $field);
        }

        return parent::getChildComponents();
    }
}
