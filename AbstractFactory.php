<?php

namespace Teqt\LandingPages;

use Magento\Framework\ObjectManagerInterface;
use Teqt\LandingPages\Exception\ClassNotFoundException;

abstract class AbstractFactory
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * AnswerInterfaceFactory constructor.
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @return mixed
     * @throws ClassNotFoundException
     */
    public function create()
    {
        $context = $this->context();
        if(! isset($context) || (! class_exists($context) && ! interface_exists($context)))
        {
            throw new ClassNotFoundException(sprintf("Class or type '%s' not found.", $context));
        }

        return $this->objectManager->create($context);
    }

    /**
     * @return string
     */
    abstract public function context();
}