# Magento 2 Landing pages

## Intent of this module

This project is intended to create (SEO optimized) catalog pages for specific attribute combinations.

## Support

Need any help installing this plugin, or to make it fit your specific webshop? Feel free to [contact me](mailto:teqt@teqt.nl), and we'll see what we can do.

## Getting Started

These instructions will install this plugin into your Magento2 installation.

### Installing

Just install through composer:

```shell
~# composer config repositories.magento2-landingpages vcs git@bitbucket.org:teqt/magento2-landingpages.git
~# composer require teqt/magento2-landingpages
```

Enable this module:

```shell
~# ./bin/magento module:enable Teqt_LandingPages
```

Clear Magento cache and upgrade your setup:

```shell
~# ./bin/magento cache:flush
~# ./bin/magento setup:upgrade
```

Define what attributes you would like to use for your landing pages and enable the frontend, in 'Stores' -> 'Settings' -> 'Configuration', under 'General' -> 'Content management' -> 'Landing pages'

### End result

Login to the admin, and you should now have an additional option 'Landing pages' under 'Content' -> 'Elements'. Have fun creating your pages.

## Bugfixes and feature requests

Let's make this the best plugin the world has ever seen. Just [tell me](mailto:teqt@teqt.nl) about your ideas, or send in a pull request.
